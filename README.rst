.. image:: https://bitbucket.org/capraconsulting-ondemand/casana/raw/0ccd33f05814433dd343b865945c2eb7c39ff8dc/casana-logo.png
   :target: http://www.capraconsulting.no

========
Overview
========

CASANA is a API which can integrate your java application with ASANA

Features
========

Most of the asana features documented here: http://developer.asana.com/documentation are supported.

Dependencies
============

Casana makes use of as few libraries as possible, with the following dependencies:

* `google-http-client`_ 1.17.0-rc
* `google-http-client-jackson2`_ 1.17.0-rc
* `google-guava`_ 15.0

Installation
============

In order to start work with casana you will need gradle build systsem.
http://www.gradle.org/::

    $ cd PATH_TO_CASANA

Simply build your casana jar file with following command::

    $ gradle build

casana-version.jar file will be placed under: build/libs

The only thing you should get in order to start working with casana is an ApiKey. You will find it in app.asana.com.

Examples usage of CASANA
============

Asking for user data::

    $ String asanaApiKey = "your key here ...";
    $ AsanaClient asanaClient = new AsanaClient(asanaApiKey);
    $ AsanaUser asanaUser = asanaClient.getMe();

Getting list of projects (which are available for the user)::

    $ String asanaApiKey = "your key here ...";
    $ AsanaClient asanaClient = new AsanaClient(asanaApiKey);
    $ List<AsanaProject> projects =  asanaClient.getProjects();


As you see casana let you work with very simple api. But asana has also more advanced options and this is still possible
with casana but requires you will use more advanced part of the api - which is very generic and extensible.

More examples with some advanced API usage
============

Asking for user data::

    $ asanaHttpClient = new AsanaHttpClient(new AsanaConfiguration("your api key"));
    $ AsanaUser me = asanaHttpClient.get(AsanaUser.class, SimpleAsanaUrlGenerator.me());
    $ Assert.assertTrue(me.getEmail(), "cki@capraconsulting.no");

Creating a task::

    $ long workspaceId = 1l;
    $ AsanaTaskCreate asanaTaskCreate = new AsanaTaskCreate(workspaceId, "casana task");
    $ AsanaTask asanaTask = asanaHttpClient.put(AsanaTask.class, asanaTaskCreate, SimpleAsanaUrlGenerator.tasks());
    $ Assert.assertNotNull(asanaTask);

.. note::

    More examples you will find in test module of the project.

    Please note that tests are using some default configuration. Configuration is placed inside::

    $ com.capra.integration.asana.TestConfigurationSource

    The origin of this configuration is on boarding application (aka Capra Ninja).


Created by `Łukasz Kaleta <lka@capraconsulting.no>`_
