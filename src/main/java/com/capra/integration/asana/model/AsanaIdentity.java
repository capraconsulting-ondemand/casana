package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

/**
 * Object which represent existing asana data.
 * In many cases asana objects are following pattern of id and name.
 *
 * @author lka@capraconsulting.no
 * @since 13-09-2013 12:44
 */
public class AsanaIdentity {

    public static final String NAME = "name";
    public static final String ID = "id";

    //
    // Some common properties not directly used by asana identity, but used by many objects.
    //
    public static final String WORKSPACE = "workspace";
    public static final String WORKSPACES = "workspaces";
    public static final String CREATED_AT = "created_at";
    public static final String MODIFIED_AT = "modified_at";
    public static final String COLOR = "color";
    public static final String FOLLOWERS = "followers";

    @Key(ID)
    private Long id;

    @Key(NAME)
    private String name;

    public AsanaIdentity() {}

    public AsanaIdentity(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
