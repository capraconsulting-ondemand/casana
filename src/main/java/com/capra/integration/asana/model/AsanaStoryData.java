package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

/**
 * @author lka@capraconsulting.no
 * @since 10-11-2013 10:37
 */
public class AsanaStoryData implements AsanaData<AsanaStory> {

    @Key(DATA)
    private AsanaStory story;

    @Override
    public AsanaStory getData() {
        return story;
    }

    public AsanaStory getStory() {
        return getData();
    }
}
