package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * Full information about asana user.
 *
 * @author lka@capraconsulting.no
 * @since 13-09-2013 14:04
 */
@AsanaDataType(AsanaUserData.class)
public class AsanaUser extends AsanaIdentity {

    public static final String EMAIL = "email";
    public static final String PHOTO = "photo";

    @Key(EMAIL)
    private String email;

    @Key(WORKSPACES)
    private List<AsanaIdentity> workspaces;

    @Key(PHOTO)
    private AsanaPhoto asanaPhoto;

    public String getEmail() {
        return email;
    }

    public List<AsanaIdentity> getWorkspaces() {
        return workspaces;
    }

    public AsanaPhoto getAsanaPhoto() {
        return asanaPhoto;
    }
}
