package com.capra.integration.asana.model;

/**
 * Contains all valid colors for tags, projects in asana.
 *
 * When someone will use other color then "400 Bad Request" error is thrown.
 *
 * @author lka@capraconsulting.no
 * @since 23-09=2013 10:30
 */
public enum AsanaValidColor {
    
    DARK_PINK,
    DARK_GREEN,
    DARK_BLUE,
    DARK_RED,
    DARK_TEAL,
    DARK_BROWN,
    DARK_ORANGE,
    DARK_PURPLE,
    DARK_WARM_GRAY,
    LIGHT_PINK,
    LIGHT_GREEN,
    LIGHT_BLUE,
    LIGHT_RED,
    LIGHT_TEAL,
    LIGHT_YELLOW,
    LIGHT_ORANGE,
    LIGHT_PURPLE,
    LIGHT_WARM_GRAY;

    /**
     * Return name which is known to asana, so it will not complain with 400 error.
     *
     * @return lower case name with underscore as word separator, ie: dark_pink.
     */
    public String getName() {
        return name().toLowerCase().replace("_", "-");
    }

    public static AsanaValidColor getPreferredColor() {
        return DARK_ORANGE;
    }
}
