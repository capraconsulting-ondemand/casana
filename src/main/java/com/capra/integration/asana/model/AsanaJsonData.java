package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

/**
 * This class can be used for passing asana PUT requests.
 *
 * It may also be helpful for analyze asana responses.
 * Example:
 * In case you do not know what to expect from asana request, you may use this object as a safe one.
 * AsanaHttpClient.get(AsanaJsonData.class, SimpleAsanaUrlGenerator.me())
 *
 * @author lka@capraconsulting.no
 * @since 13-09-2013 12:13
 */
public class AsanaJsonData<D> implements AsanaData<D> {

    @Key
    private D data;

    public AsanaJsonData() {
        // Default constructor need for JSON.
    }

    public AsanaJsonData(D data) {
        this.data = data;
    }

    public D getData() {
        return data;
    }
}
