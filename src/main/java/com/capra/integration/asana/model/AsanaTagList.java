package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * @author lka@capraconsulting.no
 * @since 27-09-2013 12:02
 */
public class AsanaTagList implements AsanaData<List<AsanaTag>> {

    @Key(DATA)
    private List<AsanaTag> asanaTags;

    public List<AsanaTag> getAsanaTags() {
        return asanaTags;
    }

    @Override
    public List<AsanaTag> getData() {
        return getAsanaTags();
    }
}
