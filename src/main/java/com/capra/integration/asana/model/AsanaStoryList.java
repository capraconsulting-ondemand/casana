package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * @author lka@capraconsulting.no
 * @since 10-11-2013 10:45
 */
public class AsanaStoryList implements AsanaData<List<AsanaStory>> {

    @Key(DATA)
    private List<AsanaStory> stories;

    @Override
    public List<AsanaStory> getData() {
        return stories;
    }

    public List<AsanaStory> getStories() {
        return stories;
    }
}
