package com.capra.integration.asana.model;

import java.lang.annotation.ElementType;

/**
 * @author lka@capraconsulting.no
 * @since 19-09-2013 14:37
 */
@java.lang.annotation.Target({ElementType.TYPE})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface AsanaDataType {

    Class value();
}
