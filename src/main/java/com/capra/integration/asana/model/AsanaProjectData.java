package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

/**
 * @author lka@capraconsulting.no
 * @since 13-09-2013 12:24
 */
public class AsanaProjectData implements AsanaData<AsanaProject> {

    @Key(DATA)
    private AsanaProject project;

    public AsanaProject getData() {
        return getProject();
    }

    public AsanaProject getProject() {
        return project;
    }
}
