package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

/**
 * @author lka@capraconsulting.no
 * @since 10-11-2013 10:48
 */
public class AsanaStoryCreate {

    private final long taskId;

    @Key
    private final String text;

    public AsanaStoryCreate(long taskId, String text) {
        this.taskId = taskId;
        this.text = text;
    }

    public long getTaskId() {
        return taskId;
    }

    public String getText() {
        return text;
    }
}
