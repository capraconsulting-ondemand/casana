package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * @author lka@capraconsulting.no
 * @since 23-09-2013 09:45
 */
@AsanaDataType(AsanaTagData.class)
public class AsanaTag extends AsanaIdentity {

    public static final String NOTES = "notes";

    @Key(CREATED_AT)
    private String createdAt;

    @Key(NOTES)
    private String notes;

    @Key(WORKSPACE)
    private AsanaIdentity workspace;

    @Key(COLOR)
    private String color;

    @Key(FOLLOWERS)
    private List<AsanaIdentity> followers;

    public String getCreatedAt() {
        return createdAt;
    }

    public String getNotes() {
        return notes;
    }

    public AsanaIdentity getWorkspace() {
        return workspace;
    }

    public String getColor() {
        return color;
    }

    public List<AsanaIdentity> getFollowers() {
        return followers;
    }
}
