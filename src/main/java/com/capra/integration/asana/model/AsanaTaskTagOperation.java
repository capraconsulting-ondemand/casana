package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

/**
 * This object can be used in:
 * - Add tag to a task
 * - Remove tag from a task
 *
 * @author lka@capraconsulting.no
 * @since 23-09-2013 10:17
 */
public class AsanaTaskTagOperation {

    @Key
    private Long tag;

    public AsanaTaskTagOperation(Long tag) {
        this.tag = tag;
    }
}

