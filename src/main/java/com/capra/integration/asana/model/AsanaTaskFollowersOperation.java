package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

import java.util.Collections;
import java.util.List;

/**
 * This object can be used for:
 * - Add followers to a task
 * - Remove followers from a task.
 *
 * @author lka@capraconsulting.no
 * @since 23-09-2013 11:04
 */
public class AsanaTaskFollowersOperation {

    @Key
    private List<Long> followers;

    public AsanaTaskFollowersOperation(Long follower) {
        this(Collections.singletonList(follower));
    }

    public AsanaTaskFollowersOperation(List<Long> followers) {
        this.followers = followers;
    }
}
