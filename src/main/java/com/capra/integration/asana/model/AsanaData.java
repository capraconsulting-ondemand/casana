package com.capra.integration.asana.model;

/**
 * @author lka@capraconsulting.no
 * @since 19-09-2013 14:31
 */
public interface AsanaData<D> {

    public static final String DATA = "data";

    public D getData();
}
