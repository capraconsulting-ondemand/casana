package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

/**
 * @author lka@capraconsulting.no
 * @since 23-09-2013 09:45
 */
public class AsanaTagData implements AsanaData<AsanaTag> {

    @Key(DATA)
    private AsanaTag tag;

    public AsanaTag getTag() {
        return tag;
    }

    @Override
    public AsanaTag getData() {
        return getTag();
    }
}
