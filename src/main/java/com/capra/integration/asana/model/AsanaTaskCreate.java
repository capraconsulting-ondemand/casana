package com.capra.integration.asana.model;

import com.google.api.client.util.Key;
import com.capra.integration.asana.utils.AsanaDate;

import java.util.Date;
import java.util.List;

/**
 * Object for creating asana task.
 *
 * @author lka@capraconsulting.no
 * @since 13-09-2013 12:41
 */
public class AsanaTaskCreate {

    @Key
    private final Long workspace;

    @Key
    private final String name;

    @Key
    private List<Long> projects;

    @Key
    private Long assignee;

    @Key
    private List<Long> followers;

    @Key
    private String notes;

    @Key(AsanaTaskData.DUE_ON)
    private String dueOn;

    public AsanaTaskCreate(Long workspaceId, String name) {
        this.workspace = workspaceId;
        this.name = name;
    }

    public void setProjects(List<Long> projects) {
        this.projects = projects;
    }

    public void setAssignee(Long assignee) {
        this.assignee = assignee;
    }

    public void setFollowers(List<Long> followers) {
        this.followers = followers;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setDueOn(Date dueOn) {
        this.dueOn = AsanaDate.format(dueOn);
    }
}
