package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * Project list when used with SimpleAsanaUrlGenerator will return only name and id.
 * By using it with AdvancedAsanaUrlGenerator and withFields() method content of the projects inside list can be adjusted.
 *
 * @author lka@capraconsulting.no
 * @since 27-09-2013 11:13
 */
public class AsanaProjectList implements AsanaData<List<AsanaProject>> {

    @Key(DATA)
    private List<AsanaProject> projects;

    public List<AsanaProject> getProjects() {
        return projects;
    }

    @Override
    public List<AsanaProject> getData() {
        return getProjects();
    }
}
