package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * User list when used with SimpleAsanaUrlGenerator will return only name and id.
 * By using it with AdvancedAsanaUrlGenerator and withFields() method content of the users inside list can be adjusted.
 *
 * @author lka@capraconsulting.no
 * @since 27-09-2013 12:06
 */
public class AsanaUserList implements AsanaData<List<AsanaUser>> {

    @Key(DATA)
    private List<AsanaUser> users;

    public List<AsanaUser> getUsers() {
        return users;
    }

    @Override
    public List<AsanaUser> getData() {
        return getUsers();
    }
}
