package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

/**
 * Object used for creating new project.
 *
 * @author lka@capraconsulting.no
 * @since 13-09-2013 12:31
 */
public class AsanaProjectCreate {
    
    @Key
    private final String name;
    
    @Key
    private final Long team;
    
    @Key
    private final Long workspace;

    @Key
    private String color;

    public AsanaProjectCreate(String name, Long team, Long workspace) {
        this.name = name;
        this.team = team;
        this.workspace = workspace;
    }

    public String getName() {
        return name;
    }

    public Long getTeam() {
        return team;
    }

    public Long getWorkspace() {
        return workspace;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setValidColor(AsanaValidColor asanaValidColor) {
        setColor(asanaValidColor.getName());
    }
}
