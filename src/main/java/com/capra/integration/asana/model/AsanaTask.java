package com.capra.integration.asana.model;

import com.google.api.client.util.Key;
import com.capra.integration.asana.utils.AsanaDate;

import java.util.Date;
import java.util.List;

/**
 * @author lka@capraconsulting.no
 * @since 13-09-2013 13:33
 */
@AsanaDataType(AsanaTaskData.class)
public class AsanaTask extends AsanaIdentity {

    public static final String COMPLETED = "completed";
    public static final String COMPLETED_AT = "completed_at";
    public static final String DUE_ON = "due_on";
    public static final String PROJECTS = "projects";

    @Key
    private AsanaIdentity assignee;

    @Key(value = "assignee_status")
    private String assigneeStatus;

    @Key(CREATED_AT)
    private String createdAt;

    @Key(COMPLETED)
    private boolean completed;

    @Key(COMPLETED_AT)
    private String completedAt;

    @Key(DUE_ON)
    private String dueOn;

    @Key(FOLLOWERS)
    private List<AsanaIdentity> followers;

    @Key(MODIFIED_AT)
    private String modifiedAt;

    @Key
    private String notes;

    @Key(PROJECTS)
    private List<AsanaIdentity> projects;

    @Key
    private AsanaIdentity parent;

    @Key
    private AsanaIdentity workspace;

    @Key
    private List<AsanaIdentity> tags;

    public AsanaIdentity getAssignee() {
        return assignee;
    }

    public String getAssigneeStatus() {
        return assigneeStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public boolean isCompleted() {
        return completed;
    }

    public String getCompletedAt() {
        return completedAt;
    }

    public String getDueOn() {
        return dueOn;
    }

    public Date getDueOnDate() {
        return AsanaDate.parse(dueOn);
    }

    public List<AsanaIdentity> getFollowers() {
        return followers;
    }

    public String getModifiedAt() {
        return modifiedAt;
    }

    public String getNotes() {
        return notes;
    }

    public List<AsanaIdentity> getProjects() {
        return projects;
    }

    public AsanaIdentity getParent() {
        return parent;
    }

    public AsanaIdentity getWorkspace() {
        return workspace;
    }

    public List<AsanaIdentity> getTags() {
        return tags;
    }
}
