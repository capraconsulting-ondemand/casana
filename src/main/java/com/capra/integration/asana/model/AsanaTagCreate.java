package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

/**
 * @author lka@capraconsulting.no
 * @since 23-09-2013 09:45
 */
public class AsanaTagCreate {

    @Key
    private final String name;

    @Key
    private final long workspace;

    @Key
    private String color;

    @Key
    private String notes;

    public AsanaTagCreate(String name, long workspace) {
        this.name = name;
        this.workspace = workspace;
    }

    public String getName() {
        return name;
    }

    public long getWorkspace() {
        return workspace;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setValidColor(AsanaValidColor asanaValidColor) {
        setColor(asanaValidColor.getName());
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
