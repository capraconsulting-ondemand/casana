package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

/**
 * This object can be used for
 * - Add project to a task.
 * - Removing project from a task.
 *
 * @author lka@capraconsulting.no
 * @since 23-09-2013 11:01
 */
public class AsanaTaskProjectOperation {

    @Key
    private Long project;

    public AsanaTaskProjectOperation(Long project) {
        this.project = project;
    }
}
