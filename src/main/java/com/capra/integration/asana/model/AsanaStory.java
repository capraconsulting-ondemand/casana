package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

/**
 * @author lka@capraconsulting.no
 * @since 10-11-2013 10:37
 */
@AsanaDataType(AsanaStoryData.class)
public class AsanaStory {

    @Key
    private Long id;

    @Key("created_at")
    private String createdAt;

    @Key("created_by")
    private AsanaIdentity createdBy;

    @Key
    private AsanaIdentity target;

    @Key
    private String source;

    @Key
    private String type;

    @Key
    private String text;

    public AsanaSource getAsanaSource() {
        return AsanaSource.valueOfSource(source);
    }

    public Long getId() {
        return id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public AsanaIdentity getCreatedBy() {
        return createdBy;
    }

    public String getText() {
        return text;
    }

    public AsanaIdentity getTarget() {
        return target;
    }

    public String getSource() {
        return source;
    }

    public String getType() {
        return type;
    }
}
