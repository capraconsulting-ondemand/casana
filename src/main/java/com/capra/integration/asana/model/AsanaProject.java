package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * @author lka@capraconsulting.no
 * @since 13-09-2013 11:26
 */
@AsanaDataType(AsanaProjectData.class)
public class AsanaProject extends AsanaIdentity {

    @Key(CREATED_AT)
    private String createdAt;

    @Key(MODIFIED_AT)
    private String modifiedAt;

    @Key
    private String notes;

    @Key
    private boolean archived;

    @Key
    private AsanaIdentity workspace;

    @Key
    private AsanaIdentity team;

    @Key
    private String color;

    @Key
    private List<AsanaIdentity> followers;

    public AsanaProject() {
    }

    public AsanaProject(Long id) {
        super(id);
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getModifiedAt() {
        return modifiedAt;
    }

    public String getNotes() {
        return notes;
    }

    public boolean isArchived() {
        return archived;
    }

    public AsanaIdentity getWorkspace() {
        return workspace;
    }

    public AsanaIdentity getTeam() {
        return team;
    }

    public String getColor() {
        return color;
    }

    public List<AsanaIdentity> getFollowers() {
        return followers;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setModifiedAt(String modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public void setWorkspace(AsanaIdentity workspace) {
        this.workspace = workspace;
    }

    public void setTeam(AsanaIdentity team) {
        this.team = team;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setFollowers(List<AsanaIdentity> followers) {
        this.followers = followers;
    }
}
