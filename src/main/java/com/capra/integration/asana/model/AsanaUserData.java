package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

/**
 * Object which represent data of single user.
 * Data contains full user information.
 *
 * @see AsanaUser
 *
 * @author lka@capraconsulting.no
 * @since 13-09-2013 14:13
 */
public class AsanaUserData implements AsanaData {

    @Key(DATA)
    private AsanaUser user;

    public AsanaUser getUser() {
        return user;
    }

    @Override
    public AsanaUser getData() {
        return getUser();
    }
}
