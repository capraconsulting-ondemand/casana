package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * Task list when used with SimpleAsanaUrlGenerator will return only name and id.
 * By using it with AdvancedAsanaUrlGenerator and withFields() method content of the tasks inside list can be adjusted.
 *
 * @author lka@capraconsulting.no
 * @since 27-09-2013 11:28
 */
public class AsanaTaskList implements AsanaData<List<AsanaTask>> {

    @Key(DATA)
    private List<AsanaTask> tasks;

    public List<AsanaTask> getTasks() {
        return tasks;
    }

    @Override
    public List<AsanaTask> getData() {
        return getTasks();
    }
}
