package com.capra.integration.asana.model;

/**
 * @author lka@capraconsulting.no
 * @since 10-11-2013 10:40
 */
public enum  AsanaSource {

    WEB,

    EMAIL,

    MOBILE,

    API,

    UNKNOWN;

    public String getName() {
        return name().toLowerCase();
    }

    public static AsanaSource valueOfSource(String source) {
        try {
            return valueOf(source.toUpperCase());
        } catch (Exception e) {
            return UNKNOWN;
        }
    }
}
