package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

/**
 * @author lka@capraconsulting.no
 * @since 19-09-2013 13:50
 */
public class AsanaPhoto {

    @Key
    private String image_21x21;

    @Key
    private String image_27x27;

    @Key
    private String image_36x36;

    @Key
    private String image_60x60;

    @Key
    private String image_128x128;

    public String getImage_21x21() {
        return image_21x21;
    }

    public String getImage_27x27() {
        return image_27x27;
    }

    public String getImage_36x36() {
        return image_36x36;
    }

    public String getImage_60x60() {
        return image_60x60;
    }

    public String getImage_128x128() {
        return image_128x128;
    }
}
