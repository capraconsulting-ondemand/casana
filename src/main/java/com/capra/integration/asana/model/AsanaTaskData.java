package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

/**
 * @author lka@capraconsulting.no
 * @since 13-09-2013 13:30
 */
public class AsanaTaskData implements AsanaData<AsanaTask> {

    public static final String DUE_ON = "due_on";

    @Key(DATA)
    private AsanaTask task;

    public AsanaTask getTask() {
        return task;
    }

    @Override
    public AsanaTask getData() {
        return getTask();
    }
}
