package com.capra.integration.asana.model;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * Generic list object for AsanaIdentity list.
 * One can get any kind of the list with asana objects: projects, tasks, etc
 *
 * When more data is required please use dedicated list object, ie AsanaProjectList
 *
 * @author lka@capraconsulting.no
 * @since 19-09-2013 21:32
 */
public class AsanaList implements AsanaData<List<AsanaIdentity>> {

    @Key(DATA)
    private List<AsanaIdentity> asanaObjects;

    @Override
    public List<AsanaIdentity> getData() {
        return asanaObjects;
    }
}
