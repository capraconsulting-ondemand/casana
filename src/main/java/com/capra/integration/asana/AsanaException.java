package com.capra.integration.asana;


/**
 * Basic runtime exception which is thrown by asana client when some communication error occurs.
 *
 * @author lka@capraconsulting.no
 * @since 13-09-2013 18:08
 */
public class AsanaException extends RuntimeException {

    public AsanaException(String errorMessage, Exception e) {
        super(errorMessage, e);
    }
}
