package com.capra.integration.asana.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Simple utility class for transforming dates which comes in/from asana.
 *
 * @author lka@capraconsulting.no
 * @since 17-09-2013 12:40
 */
public class AsanaDate {

    private static final Logger log = LoggerFactory.getLogger(AsanaDate.class);

    public static String format(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public static Date parse(String date) {
        // remove time zone
        if (date == null) {
            return null;
        }
        if (date.endsWith("Z")) {
            date = date.substring(0, date.lastIndexOf("."));
            try {
                return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(date);
            } catch (ParseException e) {
                log.error(e.getMessage(), e);
                return null;
            }
        } else if (date.length() == "yyyy-MM-dd".length()) {
            try {
                return new SimpleDateFormat("yyyy-MM-dd").parse(date);
            } catch (ParseException e) {
                log.error(e.getMessage(), e);
                return null;
            }
        }

        return null;
    }
}
