package com.capra.integration.asana.utils;

import com.google.common.base.Function;
import com.capra.integration.asana.model.AsanaIdentity;

import javax.annotation.Nullable;

/**
 * Function which gets id from AsanaIdentity.
 *
 * @author lka@capraconsulting.no
 * @since 23-09-2013 14:27
 */
public class AsanaIdentityIdFunction implements Function<AsanaIdentity, Long> {

    @Nullable
    @Override
    public Long apply(@Nullable AsanaIdentity asanaIdentity) {
        if (asanaIdentity == null) {
            return null;
        } else {
            return asanaIdentity.getId();
        }
    }
}
