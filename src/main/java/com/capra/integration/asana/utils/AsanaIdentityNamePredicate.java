package com.capra.integration.asana.utils;

import com.google.common.base.Predicate;
import com.capra.integration.asana.model.AsanaIdentity;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Set;

/**
 * Predicate which is useful when we need to find asana identity object by its name.
 *
 * @author lka@capraconsulting.no
 * @since 13-09-2013 12:50
 */
public class AsanaIdentityNamePredicate implements Predicate<AsanaIdentity> {

    /** Name of the asana identity object. */
    private final Set<String> names;


    /**
     * Construct predicate with expected name.
     * This name will be used to compare incoming asana identity objects.
     *
     * @param name of the expected asana identity object.
     */
    public AsanaIdentityNamePredicate(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Please provide not null name for asana identity name predicate");
        }
        this.names = Collections.singleton(name);
    }

    public AsanaIdentityNamePredicate(Set<String> names) {
        if (names == null) {
            throw new IllegalArgumentException("Please provide not null set of names for asana identity name predicator");
        }
        this.names = names;
    }

    @Override
    public boolean apply(@Nullable AsanaIdentity asanaIdentity) {
        return asanaIdentity != null && names.contains(asanaIdentity.getName());
    }
}
