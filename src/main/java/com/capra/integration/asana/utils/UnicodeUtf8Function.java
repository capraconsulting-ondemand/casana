package com.capra.integration.asana.utils;

import com.google.common.base.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.UnsupportedEncodingException;

/**
 * @author lka@capraconsulting.no
 * @since 25-09-2013 09:54
 */
public class UnicodeUtf8Function implements Function<String, String> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Nullable
    @Override
    public String apply(@Nullable String input) {
        if (input != null) {
            if (input.contains("\\u")) {
                try {
                    byte[] converttoBytes = input.getBytes("UTF-8");
                    return new String(converttoBytes, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    log.info(String.format("Can not convert <%s> into UTF-8: %s", input, e.getMessage()), e);
                }
            } else {
                return input;
            }
        }
        return null;
    }
}
