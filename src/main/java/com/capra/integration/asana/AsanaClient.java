package com.capra.integration.asana;

import com.capra.integration.asana.http.AsanaGoogleHttpClient;
import com.capra.integration.asana.http.AsanaHttpClient;
import com.capra.integration.asana.http.SimpleAsanaUrlGenerator;
import com.capra.integration.asana.model.*;

import java.util.*;

/**
 * @author lka@capraconsulting.no
 * @since 08-11-2013 15:43
 */
public class AsanaClient {

    private final AsanaHttpClient asanaHttpClient;

    public AsanaClient(String asanaApiKey) {
        this(new AsanaConfiguration(asanaApiKey));
    }

    public AsanaClient(AsanaConfiguration asanaConfiguration) {
        asanaHttpClient = new AsanaGoogleHttpClient(asanaConfiguration);
    }

    public AsanaClient(AsanaHttpClient asanaHttpClient) {
        this.asanaHttpClient = asanaHttpClient;
    }

    public AsanaHttpClient getHttpClient() {
        return asanaHttpClient;
    }

    public AsanaUser getUser(long userId) {
        return asanaHttpClient.get(AsanaUser.class, SimpleAsanaUrlGenerator.user(userId));
    }

    public AsanaUser getMe() {
        return asanaHttpClient.get(AsanaUser.class, SimpleAsanaUrlGenerator.me());
    }

    public List<AsanaUser> getUsers() {
        AsanaUserList asanaUserList = asanaHttpClient.get(AsanaUserList.class, SimpleAsanaUrlGenerator.users());
        return asanaUserList == null ? Collections.<AsanaUser>emptyList() : asanaUserList.getUsers();
    }

    public List<AsanaUser> getWorkspaceUsers(long workspaceId) {
        AsanaUserList asanaUserList = asanaHttpClient.get(AsanaUserList.class, SimpleAsanaUrlGenerator.workspaceUsers(workspaceId));
        return asanaUserList == null ? Collections.<AsanaUser>emptyList() : asanaUserList.getUsers();
    }

    public List<AsanaTask> getTasks() {
        AsanaTaskList asanaTaskList = asanaHttpClient.get(AsanaTaskList.class, SimpleAsanaUrlGenerator.tasks());
        return asanaTaskList == null ? Collections.<AsanaTask>emptyList() : asanaTaskList.getTasks();
    }

    public List<AsanaTask> getWorkspaceTasks(long workspaceId) {
        AsanaTaskList asanaTaskList = asanaHttpClient.get(AsanaTaskList.class, SimpleAsanaUrlGenerator.workspaceTasks(workspaceId));
        return asanaTaskList == null ? Collections.<AsanaTask>emptyList() : asanaTaskList.getTasks();
    }

    public AsanaTask createTask(AsanaTaskCreate asanaTaskCreate) {
        return asanaHttpClient.post(AsanaTask.class, asanaTaskCreate, SimpleAsanaUrlGenerator.tasks());
    }

    public AsanaTask getTask(long taskId) {
        return asanaHttpClient.get(AsanaTask.class, SimpleAsanaUrlGenerator.task(taskId));
    }

    public List<AsanaTask> getProjectTasks(long projectId) {
        AsanaTaskList asanaTaskList =  asanaHttpClient.get(AsanaTaskList.class, SimpleAsanaUrlGenerator.projectTasks(projectId));
        return asanaTaskList == null ? Collections.<AsanaTask>emptyList() : asanaTaskList.getTasks();
    }

    public List<AsanaTask> getSubTasks(long taskId) {
        AsanaTaskList asanaTaskList = asanaHttpClient.get(AsanaTaskList.class, SimpleAsanaUrlGenerator.subTasks(taskId));
        return asanaTaskList == null ? Collections.<AsanaTask>emptyList() : asanaTaskList.getTasks();
    }

    public List<AsanaStory> getTaskStories(long taskId) {
        AsanaStoryList asanaStoryList = asanaHttpClient.get(AsanaStoryList.class, SimpleAsanaUrlGenerator.taskStories(taskId));
        return asanaStoryList.getData() == null ? Collections.<AsanaStory>emptyList() : asanaStoryList.getData();
    }

    public AsanaStory createTaskStory(AsanaStoryCreate asanaStoryCreate) {
        return asanaHttpClient.post(AsanaStory.class, asanaStoryCreate, SimpleAsanaUrlGenerator.taskStories(asanaStoryCreate.getTaskId()));
    }

    public AsanaStory commentTask(Long taskId, String comment) {
        return asanaHttpClient.post(AsanaStory.class, new AsanaStoryCreate(taskId, comment),
                SimpleAsanaUrlGenerator.taskComment(taskId));
    }

    public List<AsanaProject> getTaskProjects(Long taskId) {
        AsanaProjectList asanaProjectList = asanaHttpClient.get(AsanaProjectList.class, SimpleAsanaUrlGenerator.taskProjects(taskId));
        return asanaProjectList == null ? Collections.<AsanaProject>emptyList() : asanaProjectList.getData();
    }

    public void taskAddProject(Long taskId, Long projectId) {
        asanaHttpClient.post(new AsanaTaskProjectOperation(projectId), SimpleAsanaUrlGenerator.taskAddProject(taskId));
    }

    public void taskRemoveProject(Long taskId, Long projectId) {
        asanaHttpClient.post(new AsanaTaskProjectOperation(projectId), SimpleAsanaUrlGenerator.taskAddProject(taskId));
    }

    public List<AsanaTag> getTaskTags(Long taskId) {
        AsanaTagList asanaTagList = asanaHttpClient.get(AsanaTagList.class, SimpleAsanaUrlGenerator.taskTags(taskId));
        return asanaTagList == null ? Collections.<AsanaTag>emptyList() : asanaTagList.getAsanaTags();
    }

    public AsanaTag createTag(AsanaTagCreate asanaTagCreate) {
        return asanaHttpClient.post(AsanaTag.class, asanaTagCreate, SimpleAsanaUrlGenerator.tags());
    }

    public void taskAddTag(Long taskId, Long tagId) {
        asanaHttpClient.post(new AsanaTaskTagOperation(tagId), SimpleAsanaUrlGenerator.taskAddTag(taskId));
    }

    public void taskRemoveTag(Long taskId, Long tagId) {
        asanaHttpClient.post(new AsanaTaskTagOperation(tagId), SimpleAsanaUrlGenerator.taskRemoveTag(taskId));
    }

    public void taskAddFollower(Long taskId, Long followerId, Long ... followersId) {
        List<Long> followers = new ArrayList<Long>();
        followers.add(followerId);
        if (followersId != null && followersId.length > 0) {
            followers.addAll(Arrays.asList(followersId));
        }
        taskAddFollowers(taskId, followers);
    }

    public void taskAddFollowers(Long taskId, List<Long> followerIds) {
        asanaHttpClient.post(new AsanaTaskFollowersOperation(followerIds), SimpleAsanaUrlGenerator.taskAddFollowers(taskId));
    }

    public void taskRemoveFollower(Long taskId, Long followerId) {
        asanaHttpClient.post(new AsanaTaskFollowersOperation(followerId), SimpleAsanaUrlGenerator.taskRemoveFollowers(taskId));
    }

    public AsanaProject createProject(AsanaProjectCreate asanaProjectCreate) {
        return asanaHttpClient.post(AsanaProject.class, asanaProjectCreate, SimpleAsanaUrlGenerator.projects());
    }

    public List<AsanaProject> getProjects() {
        AsanaProjectList asanaProjectList = asanaHttpClient.get(AsanaProjectList.class, SimpleAsanaUrlGenerator.projects());
        return asanaProjectList == null ? Collections.<AsanaProject>emptyList() : asanaProjectList.getProjects();
    }

    public List<AsanaProject> getWorkspaceProjects(Long workspaceId) {
        AsanaProjectList asanaProjectList = asanaHttpClient.get(AsanaProjectList.class, SimpleAsanaUrlGenerator.workspaceProjects(workspaceId));
        return asanaProjectList == null ? Collections.<AsanaProject>emptyList() : asanaProjectList.getProjects();
    }

    public AsanaProject getProject(Long projectId) {
        return asanaHttpClient.get(AsanaProject.class, SimpleAsanaUrlGenerator.project(projectId));
    }

    public List<AsanaTag> getTags() {
        AsanaTagList asanaTagList = asanaHttpClient.get(AsanaTagList.class, SimpleAsanaUrlGenerator.tags());
        return asanaTagList == null ? Collections.<AsanaTag>emptyList() : asanaTagList.getAsanaTags();
    }

    public List<AsanaTag> getWorkspaceTags(Long workspaceId) {
        AsanaTagList asanaTagList = asanaHttpClient.get(AsanaTagList.class, SimpleAsanaUrlGenerator.workspaceTags(workspaceId));
        return asanaTagList == null ? Collections.<AsanaTag>emptyList() : asanaTagList.getAsanaTags();
    }

    public AsanaTag getTag(Long tagId) {
        return asanaHttpClient.get(AsanaTag.class, SimpleAsanaUrlGenerator.tag(tagId));
    }

    public List<AsanaTask> getTagTasks(Long tagId) {
        AsanaTaskList asanaTaskList = asanaHttpClient.get(AsanaTaskList.class, SimpleAsanaUrlGenerator.tagTasks(tagId));
        return asanaTaskList == null ? Collections.<AsanaTask>emptyList() : asanaTaskList.getTasks();
    }

    public List<AsanaIdentity> getWorkspaces() {
        AsanaList asanaList = asanaHttpClient.get(AsanaList.class, SimpleAsanaUrlGenerator.workspaces());
        return asanaList == null ? Collections.<AsanaIdentity>emptyList() : asanaList.getData();
    }

    public AsanaIdentity getWorkspace(Long workspaceId) {
        return asanaHttpClient.get(AsanaIdentity.class, SimpleAsanaUrlGenerator.workspace(workspaceId));
    }

    public List<AsanaIdentity> getTeams(long organizationId) {
        AsanaList asanaList = asanaHttpClient.get(AsanaList.class, SimpleAsanaUrlGenerator.teams(organizationId));
        return asanaList == null ? Collections.<AsanaIdentity>emptyList() : asanaList.getData();
    }

    public void deleteTask(Long taskId) {
        asanaHttpClient.delete(SimpleAsanaUrlGenerator.task(taskId));
    }

    public void deleteProject(Long projectId) {
        asanaHttpClient.delete(SimpleAsanaUrlGenerator.project(projectId));
    }
}
