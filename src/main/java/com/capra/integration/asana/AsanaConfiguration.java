package com.capra.integration.asana;

/**
 * Configuration for asana client.
 *
 * @author lka@capraconsulting.no
 * @since 13-09-2013 10:13
 */
public class AsanaConfiguration {

    /**
     * Security parameter.
     * This api key will determine user which invokes actions.
     */
    private final String userApiKey;

    public AsanaConfiguration(String userApiKey) {
        this.userApiKey = userApiKey;
    }

    public String getUserApiKey() {
        return userApiKey;
    }
}
