package com.capra.integration.asana.http;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lka@capraconsulting.no
 * @since 14-09-2013 22:53
 */
public class AsanaUrlOptions {

    private final String url;

    private List<String> parameters = new ArrayList<String>();

    public AsanaUrlOptions(String url) {
        this.url = url;
    }

    public AsanaUrlOptions withFields(String field, String ... fields) {
        if (field == null) {
            return this;
        }
        String optFields = "opt_fields=" + field;
        if (fields != null) {
            for (String f : fields) {
                optFields += "," + f;
            }
            optFields = optFields.substring(0, optFields.length() - 1);
        }
        parameters.add(optFields);
        return this;
    }
}
