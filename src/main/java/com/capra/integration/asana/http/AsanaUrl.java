package com.capra.integration.asana.http;

import com.google.api.client.http.GenericUrl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author lka@capraconsulting.no
 * @since 16-09-2013 08:51
 */
public class AsanaUrl {

    private final String url;

    private final Map<String, String> options = new HashMap<String, String>();

    public AsanaUrl(String url) {
        this.url = url;
    }

    public GenericUrl build() {
        StringBuilder finalUrl = new StringBuilder(url);
        if (!options.isEmpty()) {
            Set<Map.Entry<String, String>> optionEntries = options.entrySet();
            finalUrl.append("?");
            for (Map.Entry<String, String> optionEntry : optionEntries) {
                finalUrl.append(optionEntry.getKey());
                finalUrl.append("=");
                finalUrl.append(optionEntry.getValue());
                finalUrl.append("&");
            }
            finalUrl = new StringBuilder(finalUrl.substring(0, finalUrl.length() - 1));
        }

        return new GenericUrl(finalUrl.toString());
    }

    public AsanaUrl withFields(String ... fields) {
        if (fields == null || fields.length == 0) {
            return this;
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (String field : fields) {
            stringBuilder.append(field);
            stringBuilder.append(",");
        }
        String optFields = stringBuilder.substring(0, stringBuilder.length() - 1);
        options.put("opt_fields", optFields);

        return this;
    }

    public AsanaUrl meAssignee() {
        return addStringToUrl("assignee", "me");
    }

    public AsanaUrl assignee(Long assignee) {
        return addLongToUrl("assignee", assignee);
    }

    public AsanaUrl workspace(Long workspace) {
        return addLongToUrl("workspace", workspace);
    }

    public AsanaUrl expand(String optionExpand) {
        return addStringToUrl("options.expand", "*" + optionExpand + "*");
    }

    private AsanaUrl addStringToUrl(String key, String value) {
        if (value != null) {
            options.put(key, value);
        }
        return this;
    }

    private AsanaUrl addLongToUrl(String key, Long value) {
        if (value != null && value > 0) {
            options.put(key, String.valueOf(value));
        }
        return this;
    }
}
