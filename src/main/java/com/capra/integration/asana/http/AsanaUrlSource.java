package com.capra.integration.asana.http;

/**
 * @author lka@capraconsulting.no
 * @since 16-09-2013 09:06
 */
public class AsanaUrlSource {

    // USERS

    public String users() {
        return asanaUrl("/users");
    }

    public String user(Long userId) {
        return asanaUrl("/users/" + String.valueOf(userId));
    }

    public String me() {
        return asanaUrl("/users/me");
    }

    public String workspaceUsers(Long workspaceId) {
        return asanaWorkspaceUrl(workspaceId, "users");
    }

    // TASKS

    public String tasks() {
        return asanaUrl("/tasks");
    }

    public String workspaceTasks(Long workspaceId) {
        return asanaWorkspaceUrl(workspaceId, "tasks");
    }

    public String task(Long taskId) {
        return asanaUrl("/tasks/" + String.valueOf(taskId));
    }

    public String projectTasks(Long projectId) {
        return asanaProjectUrl(projectId, "tasks");
    }

    public String subTasks(Long taskId) {
        return asanaTaskUrl(taskId, "subtasks");
    }

    public String taskStories(Long taskId) {
        return asanaTaskUrl(taskId, "stories");
    }

    public String taskProjects(Long taskId) {
        return asanaTaskUrl(taskId, "projects");
    }

    public String taskAddProject(Long taskId) {
        return asanaTaskUrl(taskId, "addProject");
    }

    public String taskRemoveProject(Long taskId) {
        return asanaTaskUrl(taskId, "removeProject");
    }

    public String taskTags(Long taskId) {
        return asanaTaskUrl(taskId, "tags");
    }

    public String taskAddTag(Long taskId) {
        return asanaTaskUrl(taskId, "addTag");
    }

    public String taskRemoveTag(Long taskId) {
        return asanaTaskUrl(taskId, "removeTag");
    }

    public String taskAddFollowers(Long taskId) {
        return asanaTaskUrl(taskId, "addFollowers");
    }

    public String taskRemoveFollowers(Long taskId) {
        return asanaTaskUrl(taskId, "removeFollowers");
    }

    // PROJECTS

    public String projects() {
        return asanaUrl("/projects");
    }

    public String workspaceProjects(Long workspaceId) {
        return asanaWorkspaceUrl(workspaceId, "projects");
    }

    public String project(Long projectId) {
        return asanaUrl("/projects/" + String.valueOf(projectId));
    }

    // TAGS

    public String tags() {
        return asanaUrl("/tags");
    }

    public String workspaceTags(Long workspaceId) {
        return asanaWorkspaceUrl(workspaceId, "tags");
    }

    public String tag(Long tagId) {
        return asanaUrl("/tags/" + String.valueOf(tagId));
    }

    public String tagTasks(Long tagId) {
        return asanaUrl("/tags/" + String.valueOf(tagId) + "/tasks");
    }

    // STORIES

    // WORKSPACES/ORGANIZATIONS

    public String workspaces() {
        return asanaUrl("/workspaces");
    }

    public String workspace(long workspaceId) {
        return asanaUrl("/workspaces/" + String.valueOf(workspaceId));
    }

    public String teams(long organizationId) {
        return asanaUrl("/organizations/" + String.valueOf(organizationId) + "/teams");
    }

    // GENERIC

    private String asanaTaskUrl(Long taskId, String urlPart) {
        return asanaUrl(String.format("/tasks/%s/%s", String.valueOf(taskId), urlPart));
    }

    private String asanaProjectUrl(Long projectId, String urlPart) {
        return asanaUrl(String.format("/projects/%s/%s", String.valueOf(projectId), urlPart));
    }

    private String asanaWorkspaceUrl(Long workspaceId, String urlPart) {
        return asanaUrl(String.format("/workspaces/%s/%s", String.valueOf(workspaceId), urlPart));
    }

    private String asanaUrl(String urlPart) {
        return "https://app.asana.com/api/1.0" + urlPart;
    }
}
