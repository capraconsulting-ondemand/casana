package com.capra.integration.asana.http;

import com.capra.integration.asana.AsanaConfiguration;
import com.google.api.client.http.GenericUrl;

/**
 * Client for asana integration.
 * Contains methods for HTTP requests.
 *
 * @author lka@capraconsulting.no
 * @since 24-09-2013 15:01
 */
public interface AsanaHttpClient {

    /**
     * This method should be used when we would like to GET data from asana.
     * We provide result we expect and url under which such result is accessible.
     *
     * Example:
     * get(AsanaUser.class, SimpleAsanaUrlGenerator.me())
     *
     * @param expectedResult class which we expect will be result from request.
     * @param genericUrl generic url.
     * @param <O> type of output - expected result type.
     * @return instance of expected result class.
     */
    <O> O get(Class<O> expectedResult, GenericUrl genericUrl);

    /**
     * This method should be used when we would like to POST data to asana and we are expecting some result from it.
     * We provide data which will be posted, expected result and url which determine where we post our data.
     *
     * @param expectedResult class which we expect will be result from request.
     * @param input for our request.
     * @param genericUrl url of our post operation.
     * @param <I> type of input.
     * @param <O> type of output.
     *
     * @return instance of expected result class.
     */
    <I, O> O post(Class<O> expectedResult, I input, GenericUrl genericUrl);

    <I, O> O put(Class<O> expectedResult, I input, GenericUrl genericUrl);

    /**
     * This method should be used when we would like to POST data to asana, but we do not expect any result
     * - or we are not interested in result.
     * We provide data which will be posted and url which determine where we post our data.
     *
     * @param input for our request.
     * @param genericUrl url of our post operation.
     * @param <I> type of input.
     */
    <I> void post(I input, GenericUrl genericUrl);

    /**
     * This method should be used when we woud liek to DELETE data inside asana.
     * Here we need only to provide url of resource we would like to delete.
     *
     * Example:
     * long taskId = ...
     * delete(SimpleAsanaUrlGenerator.task(taskId)
     *
     * @param genericUrl of resource we would like to delete.
     */
    void delete(GenericUrl genericUrl);

    /**
     * Gets configuration for our http client.
     *
     * @return asana configuration object.
     */
    AsanaConfiguration getAsanaConfiguration();
}
