package com.capra.integration.asana.http;

import com.google.api.client.http.GenericUrl;

/**
 * todo: consider renaming to just "AsanaUrlGenerator"
 *
 * @author lka@capraconsulting.no
 * @since 16-09-2013 09:05
 */
public class AdvancedAsanaUrlGenerator {

    private static final AsanaUrlSource asanaUrlSource = new AsanaUrlSource();

    // USERS

    public static AsanaUrl users() {
        return asanaUrl("/users");
    }

    public static AsanaUrl user(Long userId) {
        return asanaUrl("/users/" + String.valueOf(userId));
    }

    public static AsanaUrl me() {
        return asanaUrl("/users/me");
    }

    public static AsanaUrl workspaceUsers(Long workspaceId) {
        return asanaWorkspaceUrl(workspaceId, "users");
    }

    // TASKS

    public static AsanaUrl tasks() {
        return asanaUrl("/tasks");
    }

    public static AsanaUrl workspaceTasks(Long workspaceId) {
        return asanaWorkspaceUrl(workspaceId, "tasks");
    }

    public static AsanaUrl task(Long taskId) {
        return asanaUrl("/tasks/" + String.valueOf(taskId));
    }

    public static AsanaUrl projectTasks(Long projectId) {
        return asanaProjectUrl(projectId, "tasks");
    }

    public static AsanaUrl subTasks(Long taskId) {
        return asanaTaskUrl(taskId, "subtasks");
    }

    public static AsanaUrl taskStories(Long taskId) {
        return asanaTaskUrl(taskId, "stories");
    }

    public static AsanaUrl taskProjects(Long taskId) {
        return asanaTaskUrl(taskId, "projects");
    }

    public static AsanaUrl taskAddProject(Long taskId) {
        return asanaTaskUrl(taskId, "addProject");
    }

    public static AsanaUrl taskRemoveProject(Long taskId) {
        return asanaTaskUrl(taskId, "removeProject");
    }

    public static AsanaUrl taskTags(Long taskId) {
        return asanaTaskUrl(taskId, "tags");
    }

    public static AsanaUrl taskAddTag(Long taskId) {
        return asanaTaskUrl(taskId, "addTag");
    }

    public static AsanaUrl taskRemoveTag(Long taskId) {
        return asanaTaskUrl(taskId, "removeTag");
    }

    public static AsanaUrl taskAddFollowers(Long taskId) {
        return asanaTaskUrl(taskId, "addFollowers");
    }

    public static AsanaUrl taskRemoveFollowers(Long taskId) {
        return asanaTaskUrl(taskId, "removeFollowers");
    }

    // PROJECTS

    public static AsanaUrl projects() {
        return asanaUrl("/projects");
    }

    public static AsanaUrl workspaceProjects(Long workspaceId) {
        return asanaWorkspaceUrl(workspaceId, "projects");
    }

    public static AsanaUrl project(Long projectId) {
        return asanaUrl("/projects/" + String.valueOf(projectId));
    }

    // TAGS

    public static AsanaUrl tags() {
        return asanaUrl("/tags");
    }

    public static AsanaUrl workspaceTags(Long workspaceId) {
        return asanaWorkspaceUrl(workspaceId, "tags");
    }

    public static AsanaUrl tag(Long tagId) {
        return asanaUrl("/tags/" + String.valueOf(tagId));
    }

    public static AsanaUrl tagTasks(Long tagId) {
        return asanaUrl("/tags/" + String.valueOf(tagId) + "/tasks");
    }

    // STORIES

    public static AsanaUrl taskComment(long taskId) {
        return asanaTaskUrl(taskId, "stories");
    }

    // WORKSPACES/ORGANIZATIONS

    public static AsanaUrl workspaces() {
        return asanaUrl("/workspaces");
    }

    public static AsanaUrl workspace(long workspaceId) {
        return asanaUrl("/workspaces/" + String.valueOf(workspaceId));
    }

    public static AsanaUrl teams(long organizationId) {
        return asanaUrl("/organizations/" + String.valueOf(organizationId) + "/teams");
    }

    // GENERIC

    private static AsanaUrl asanaTaskUrl(Long taskId, String urlPart) {
        return asanaUrl(String.format("/tasks/%s/%s", String.valueOf(taskId), urlPart));
    }

    private static AsanaUrl asanaProjectUrl(Long projectId, String urlPart) {
        return asanaUrl(String.format("/projects/%s/%s", String.valueOf(projectId), urlPart));
    }

    private static AsanaUrl asanaWorkspaceUrl(Long workspaceId, String urlPart) {
        return asanaUrl(String.format("/workspaces/%s/%s", String.valueOf(workspaceId), urlPart));
    }

    private static AsanaUrl asanaUrl(String urlPart) {
        return new AsanaUrl("https://app.asana.com/api/1.0" + urlPart);
    }
}
