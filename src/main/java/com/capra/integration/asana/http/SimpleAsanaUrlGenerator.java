package com.capra.integration.asana.http;

import com.google.api.client.http.GenericUrl;

/**
 * Another way of generating asana urls.
 *
 * @author lka@capraconsulting.no
 * @since 14-09-2013 15:32
 */
public class SimpleAsanaUrlGenerator {

    // USERS

    public static GenericUrl users() {
        return AdvancedAsanaUrlGenerator.users().build();
    }

    public static GenericUrl user(Long userId) {
        return AdvancedAsanaUrlGenerator.user(userId).build();
    }

    public static GenericUrl me() {
        return AdvancedAsanaUrlGenerator.me().build();
    }

    public static GenericUrl workspaceUsers(Long workspaceId) {
        return AdvancedAsanaUrlGenerator.workspaceUsers(workspaceId).build();
    }

    // TASKS

    public static GenericUrl tasks() {
        return AdvancedAsanaUrlGenerator.tasks().build();
    }

    public static GenericUrl workspaceTasks(Long workspaceId) {
        return AdvancedAsanaUrlGenerator.workspaceTasks(workspaceId).build();
    }

    public static GenericUrl task(Long taskId) {
        return AdvancedAsanaUrlGenerator.task(taskId).build();
    }

    public static GenericUrl projectTasks(Long projectId) {
        return AdvancedAsanaUrlGenerator.projectTasks(projectId).build();
    }

    public static GenericUrl subTasks(Long taskId) {
        return AdvancedAsanaUrlGenerator.subTasks(taskId).build();
    }

    public static GenericUrl taskStories(Long taskId) {
        return AdvancedAsanaUrlGenerator.taskStories(taskId).build();
    }

    public static GenericUrl taskProjects(Long taskId) {
        return AdvancedAsanaUrlGenerator.taskProjects(taskId).build();
    }

    public static GenericUrl taskAddProject(Long taskId) {
        return AdvancedAsanaUrlGenerator.taskAddProject(taskId).build();
    }

    public static GenericUrl taskRemoveProject(Long taskId) {
        return AdvancedAsanaUrlGenerator.taskRemoveProject(taskId).build();
    }

    public static GenericUrl taskTags(Long taskId) {
        return AdvancedAsanaUrlGenerator.taskTags(taskId).build();
    }

    public static GenericUrl taskAddTag(Long taskId) {
        return AdvancedAsanaUrlGenerator.taskAddTag(taskId).build();
    }

    public static GenericUrl taskRemoveTag(Long taskId) {
        return AdvancedAsanaUrlGenerator.taskRemoveTag(taskId).build();
    }

    public static GenericUrl taskAddFollowers(Long taskId) {
        return AdvancedAsanaUrlGenerator.taskAddFollowers(taskId).build();
    }

    public static GenericUrl taskRemoveFollowers(Long taskId) {
        return AdvancedAsanaUrlGenerator.taskRemoveFollowers(taskId).build();
    }

    // PROJECTS

    public static GenericUrl projects() {
        return AdvancedAsanaUrlGenerator.projects().build();
    }

    public static GenericUrl workspaceProjects(Long workspaceId) {
        return AdvancedAsanaUrlGenerator.workspaceProjects(workspaceId).build();
    }

    public static GenericUrl project(Long projectId) {
        return AdvancedAsanaUrlGenerator.project(projectId).build();
    }

    // TAGS

    public static GenericUrl tags() {
        return AdvancedAsanaUrlGenerator.tags().build();
    }

    public static GenericUrl workspaceTags(Long workspaceId) {
        return AdvancedAsanaUrlGenerator.workspaceTags(workspaceId).build();
    }

    public static GenericUrl tag(Long tagId) {
        return AdvancedAsanaUrlGenerator.tag(tagId).build();
    }

    public static GenericUrl tagTasks(Long tagId) {
        return AdvancedAsanaUrlGenerator.tagTasks(tagId).build();
    }

    // STORIES

    public static GenericUrl taskComment(long taskId) {
        return AdvancedAsanaUrlGenerator.taskComment(taskId).build();
    }

    // WORKSPACES/ORGANIZATIONS

    public static GenericUrl workspaces() {
        return AdvancedAsanaUrlGenerator.workspaces().build();
    }

    public static GenericUrl workspace(long workspaceId) {
        return AdvancedAsanaUrlGenerator.workspace(workspaceId).build();
    }

    public static GenericUrl teams(long organizationId) {
        return AdvancedAsanaUrlGenerator.teams(organizationId).build();
    }
}
















