package com.capra.integration.asana;

import com.capra.integration.asana.http.AsanaHttpClient;
import com.capra.integration.asana.model.*;
import com.capra.integration.asana.utils.AsanaIdentityNamePredicate;
import com.capra.integration.asana.utils.TestConfigurationSource;
import com.capra.integration.asana.utils.TestDataCleanUp;
import com.google.common.collect.Collections2;
import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.capra.integration.asana.http.AdvancedAsanaUrlGenerator;
import com.capra.integration.asana.http.AsanaGoogleHttpClient;
import com.capra.integration.asana.http.SimpleAsanaUrlGenerator;

import java.util.Collection;
import java.util.Collections;

import static com.capra.integration.asana.utils.TestConfigurationSource.getTeamName;
import static com.capra.integration.asana.utils.TestConfigurationSource.getWorkspaceId;

/**
 * This test contains all possible executions for tasks in asana.
 * All tests here may be used as example for asana integration.
 *
 * @author lka@capraconsulting.no
 * @since 19-09-2013 14:13
 */
public class AsanaTasksTest {

    private final AsanaHttpClient asanaHttpClient = new AsanaGoogleHttpClient(TestConfigurationSource.getConfiguration());
    private final TestDataCleanUp testDataCleanUp = new TestDataCleanUp(asanaHttpClient);

    @Before
    public void doBeforeTest() {}

    @After
    public void doAfterTest() {
        testDataCleanUp.clean();
    }

    @Test
    public void testNorwegianSpecialCharacters() {
        Long userId = asanaHttpClient.get(AsanaUser.class, SimpleAsanaUrlGenerator.me()).getId();

        String specialCharacters = "testNorwegianSpecialCharacters(\"æ - Æ : ø - Ø : å - Å\")";
        AsanaTaskCreate asanaTaskCreate = new AsanaTaskCreate(getWorkspaceId(), generateTaskName(specialCharacters));
        asanaTaskCreate.setAssignee(userId);

        AsanaTask asanaTask = asanaHttpClient.post(AsanaTask.class, asanaTaskCreate, SimpleAsanaUrlGenerator.tasks());
        Assert.assertNotNull(asanaTask);
        testDataCleanUp.addTask(asanaTask.getId());
    }

    @Test
    public void countMineTaskBeforeAndAfterCreatingNewTask() {
        Long userId = asanaHttpClient.get(AsanaUser.class, SimpleAsanaUrlGenerator.me()).getId();
        AsanaList asanaList = asanaHttpClient.get(AsanaList.class, AdvancedAsanaUrlGenerator.tasks().workspace(getWorkspaceId()).assignee(userId).build());
        Assert.assertNotNull(asanaList);
        Assert.assertNotNull(asanaList.getData());
        int countOfTheTasksBefore = asanaList.getData().size();

        AsanaTaskCreate asanaTaskCreate = new AsanaTaskCreate(getWorkspaceId(), generateTaskName("showMineTasks"));
        asanaTaskCreate.setAssignee(userId);
        AsanaTask asanaTask = asanaHttpClient.post(AsanaTask.class, asanaTaskCreate, AdvancedAsanaUrlGenerator.tasks().withFields(AsanaTask.ID).build());
        testDataCleanUp.addTask(asanaTask.getId());

        asanaList = asanaHttpClient.get(AsanaList.class, AdvancedAsanaUrlGenerator.tasks().workspace(getWorkspaceId()).assignee(userId).build());
        Assert.assertNotNull(asanaList);
        Assert.assertNotNull(asanaList.getData());
        int countOfTheTasksAfter = asanaList.getData().size();

        Assert.assertTrue(countOfTheTasksAfter > countOfTheTasksBefore);
    }

    @Test
    public void createNewTask() {
        AsanaTaskCreate asanaTaskCreate = new AsanaTaskCreate(getWorkspaceId(), generateTaskName("createNewTask"));
        AsanaTask asanaTask = asanaHttpClient.post(AsanaTask.class, asanaTaskCreate, SimpleAsanaUrlGenerator.tasks());
        Assert.assertNotNull(asanaTask);
        testDataCleanUp.addTask(asanaTask.getId());
    }

    @Test
    public void createNewTaskAndReturnOnlyId() {
        AsanaTaskCreate asanaTaskCreate = new AsanaTaskCreate(getWorkspaceId(), generateTaskName("createNewTaskAndReturnOnlyId"));
        AsanaTask asanaTask = asanaHttpClient.post(AsanaTask.class, asanaTaskCreate, AdvancedAsanaUrlGenerator.tasks().withFields(AsanaTask.ID).build());
        Assert.assertNotNull(asanaTask);
        Assert.assertNull(asanaTask.getName());
        testDataCleanUp.addTask(asanaTask.getId());
    }

    @Test
    public void testGetTaskWithExpandedProject() {
        AsanaList asanaList = asanaHttpClient.get(AsanaList.class, SimpleAsanaUrlGenerator.teams(getWorkspaceId()));
        Collection<AsanaIdentity> temas = Collections2.filter(asanaList.getData() ,new AsanaIdentityNamePredicate(getTeamName()));
        AsanaIdentity asanaTeam = temas.iterator().next();

        AsanaProjectCreate asanaProjectCreate = new AsanaProjectCreate("some project", asanaTeam.getId(), getWorkspaceId());
        AsanaProject project = asanaHttpClient.post(AsanaProject.class, asanaProjectCreate, SimpleAsanaUrlGenerator.projects());
        testDataCleanUp.addProject(project.getId());

        AsanaTaskCreate asanaTaskCreate = new AsanaTaskCreate(getWorkspaceId(), generateTaskName("GetTaskWithExpandedProject"));
        asanaTaskCreate.setProjects(Collections.singletonList(project.getId()));
        AsanaTask asanaTask = asanaHttpClient.post(AsanaTask.class, asanaTaskCreate, SimpleAsanaUrlGenerator.tasks());
        testDataCleanUp.addTask(asanaTask.getId());

        AsanaTask asanaTaskWithExpandedProject = asanaHttpClient.get(AsanaTask.class, AdvancedAsanaUrlGenerator.task(asanaTask.getId()).expand("projects").build());
        Assert.assertNotNull(asanaTaskWithExpandedProject);
    }

    private String generateTaskName(String methodName) {
        return "TestTask:" + getClass().getSimpleName() + "#" + methodName;
    }
}
