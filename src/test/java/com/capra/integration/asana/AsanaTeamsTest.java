package com.capra.integration.asana;

import com.capra.integration.asana.http.AsanaGoogleHttpClient;
import com.capra.integration.asana.utils.TestConfigurationSource;
import com.capra.integration.asana.utils.TestDataCleanUp;
import org.junit.Assert;
import org.junit.Test;
import com.capra.integration.asana.http.SimpleAsanaUrlGenerator;
import com.capra.integration.asana.model.AsanaIdentity;
import com.capra.integration.asana.model.AsanaJsonData;
import com.capra.integration.asana.model.AsanaList;

import java.util.List;

import static com.capra.integration.asana.utils.TestConfigurationSource.getWorkspaceId;

/**
 * @author lka@capraconsulting.no
 * @since 23-09-2013 09:29
 */
public class AsanaTeamsTest {

    private AsanaGoogleHttpClient asanaHttpClient = new AsanaGoogleHttpClient(TestConfigurationSource.getConfiguration());

    private final TestDataCleanUp testDataCleanUp = new TestDataCleanUp(asanaHttpClient);


    @Test
    public void showingAllTeamsUserIsMemberOf() {
        AsanaList asanaList = asanaHttpClient.get(AsanaList.class, SimpleAsanaUrlGenerator.teams(getWorkspaceId()));
        List<AsanaIdentity> teams = asanaList.getData();
        Assert.assertNotNull(teams);
    }

    @Test
    public void showingAllRowTeamDataUserIdMemberOf() {
        AsanaJsonData asanaJsonData = asanaHttpClient.get(AsanaJsonData.class, SimpleAsanaUrlGenerator.teams(getWorkspaceId()));
        Assert.assertNotNull(asanaJsonData);
    }
}
