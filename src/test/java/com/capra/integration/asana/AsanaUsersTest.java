package com.capra.integration.asana;

import com.capra.integration.asana.http.AsanaGoogleHttpClient;
import com.capra.integration.asana.http.AsanaHttpClient;
import com.capra.integration.asana.utils.TestConfigurationSource;
import com.capra.integration.asana.utils.TestDataCleanUp;
import org.junit.Assert;
import org.junit.Test;
import com.capra.integration.asana.http.AdvancedAsanaUrlGenerator;
import com.capra.integration.asana.http.SimpleAsanaUrlGenerator;
import com.capra.integration.asana.model.*;

import java.util.List;

import static com.capra.integration.asana.utils.TestConfigurationSource.getWorkspaceId;

/**
 * This test contain all possible executions for users in asana.
 * All tests here may be used as example for asana integration.
 *
 * Please note that users data can not be modified by asana api.
 *
 * @author lka@capraconsulting.no
 * @since 19-09-2013 12:24
 */
public class AsanaUsersTest {

    private final AsanaHttpClient asanaHttpClient = new AsanaGoogleHttpClient(TestConfigurationSource.getConfiguration());

    private final TestDataCleanUp testDataCleanUp = new TestDataCleanUp(asanaHttpClient);

    @Test
    public void whatShouldIExpectWhenAskingForNonExistingUser() {
        AsanaUser userDataIsNull = asanaHttpClient.get(AsanaUser.class, SimpleAsanaUrlGenerator.user(1l));
        Assert.assertNull(userDataIsNull);
    }

    @Test
    public void showMeWithFullObject() {
        AsanaUser me = asanaHttpClient.get(AsanaUser.class, SimpleAsanaUrlGenerator.me());
        Assert.assertNotNull(me);

        Assert.assertNotNull(me.getEmail());
        Assert.assertNotNull(me.getId());
        Assert.assertNotNull(me.getName());
        List<AsanaIdentity> workspaces = me.getWorkspaces();
        Assert.assertTrue(workspaces.size() > 0);

        AsanaJsonData asanaJsonData = asanaHttpClient.get(AsanaJsonData.class, SimpleAsanaUrlGenerator.me());
        Assert.assertNotNull(asanaJsonData);
    }

    @Test
    public void showMeWithSpecifiedData() {
        AsanaUser me = asanaHttpClient.get(AsanaUser.class, AdvancedAsanaUrlGenerator.me().withFields(AsanaUser.EMAIL).build());
        Assert.assertNotNull(me);

        Assert.assertNotNull(me.getId());
        Assert.assertNotNull(me.getEmail());
        Assert.assertNull(me.getName());
        Assert.assertNull(me.getWorkspaces());
    }

    @Test
    public void showMeWithOnlyId() {
        AsanaUser me = asanaHttpClient.get(AsanaUser.class, AdvancedAsanaUrlGenerator.me().withFields(AsanaUser.ID).build());
        Assert.assertNotNull(me);

        Assert.assertNotNull(me.getId());
        Assert.assertNull(me.getEmail());
        Assert.assertNull(me.getName());
        Assert.assertNull(me.getWorkspaces());
    }

    @Test
    public void showUserById() {
//        Long userId = l;
        Long userId = asanaHttpClient.get(AsanaUser.class, AdvancedAsanaUrlGenerator.me().withFields(AsanaUser.ID).build()).getId();
        Assert.assertNotNull("Test can not be continued since user id is NULL", userId);

        AsanaUser asanaUser = asanaHttpClient.get(AsanaUser.class, AdvancedAsanaUrlGenerator.user(userId).build());
        Assert.assertNotNull(asanaUser);
        // the rest as in me handling
    }

    @Test
    public void simpleShowingAllUsersInAllWorkspaces() {
        AsanaList asanaUsers = asanaHttpClient.get(AsanaList.class, SimpleAsanaUrlGenerator.users());
        Assert.assertNotNull(asanaUsers);
        List<AsanaIdentity> users = asanaUsers.getData();
        Assert.assertNotNull(users);
        Assert.assertFalse(users.isEmpty());
    }

    @Test
    public void advancedShowingAllUsersInAllWorkspaces() {
        AsanaUserList asanaUsers = asanaHttpClient.get(AsanaUserList.class, AdvancedAsanaUrlGenerator
                .users()
                .withFields(AsanaUser.EMAIL, AsanaUser.NAME)
                .build());
        Assert.assertNotNull(asanaUsers);
        List<AsanaUser> users = asanaUsers.getData();
        Assert.assertNotNull(users);
        Assert.assertFalse(users.isEmpty());
        for (AsanaUser asanaUser : users) {
            Assert.assertNotNull(asanaUser.getEmail());
            Assert.assertNotNull(asanaUser.getName());
        }
    }

    @Test
    public void showingUsersInSingleWorkspace() {
        AsanaList asanaUsers = asanaHttpClient.get(AsanaList.class, SimpleAsanaUrlGenerator.workspaceUsers(getWorkspaceId()));
        Assert.assertNotNull(asanaUsers);
        List<AsanaIdentity> users = asanaUsers.getData();
        Assert.assertNotNull(users);
        Assert.assertFalse(users.isEmpty());
    }

    @Test(expected = AsanaException.class)
    public void showingUsersInNonExistingWorkspace() {
        asanaHttpClient.get(AsanaList.class, SimpleAsanaUrlGenerator.workspaceUsers(1l));
    }

    @Test
    public void testAsanaUserWithGenericJsonData() {
        AsanaJsonData asanaJsonData = asanaHttpClient.get(AsanaJsonData.class, SimpleAsanaUrlGenerator.me());
        Assert.assertNotNull(asanaJsonData);
    }

    @Test
    public void testGetUserPhoto36x36() {
        AsanaUser me = asanaHttpClient.get(AsanaUser.class, AdvancedAsanaUrlGenerator.me().withFields("photo.image_36x36").build());
        Assert.assertNotNull(me);

        Assert.assertNotNull(me.getId());
        Assert.assertNull(me.getEmail());
        Assert.assertNull(me.getName());
        Assert.assertNull(me.getWorkspaces());
        Assert.assertNotNull(me.getAsanaPhoto());
        Assert.assertNull(me.getAsanaPhoto().getImage_21x21());
        Assert.assertNull(me.getAsanaPhoto().getImage_27x27());
        Assert.assertNotNull(me.getAsanaPhoto().getImage_36x36());
        Assert.assertNull(me.getAsanaPhoto().getImage_60x60());
        Assert.assertNull(me.getAsanaPhoto().getImage_128x128());
    }
}
