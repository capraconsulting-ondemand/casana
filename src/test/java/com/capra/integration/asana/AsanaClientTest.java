package com.capra.integration.asana;

import com.capra.integration.asana.model.AsanaIdentity;
import com.capra.integration.asana.model.AsanaTask;
import com.capra.integration.asana.model.AsanaTaskCreate;
import com.capra.integration.asana.model.AsanaUser;
import com.capra.integration.asana.utils.TestConfigurationSource;
import com.capra.integration.asana.utils.TestDataCleanUp;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * @author lka@capraconsulting.no
 * @since 11-11-2013 09:05
 */
public class AsanaClientTest {

    private final AsanaClient asanaClient = new AsanaClient(TestConfigurationSource.getConfiguration());

    private final TestDataCleanUp testDataCleanUp = new TestDataCleanUp(asanaClient);

    @After
    public void cleanUp() {
        testDataCleanUp.clean();
    }

    @Test
    public void testTaskAddFollowers() {
        AsanaTask asanaTask = asanaClient.createTask(new AsanaTaskCreate(TestConfigurationSource.getWorkspaceId(), "task"));
        testDataCleanUp.addTask(asanaTask.getId());
        AsanaUser asanaUser = asanaClient.getMe();
        asanaClient.taskAddFollower(asanaTask.getId(), asanaUser.getId());

        asanaTask = asanaClient.getTask(asanaTask.getId());
        List<AsanaIdentity> followers = asanaTask.getFollowers();
        Assert.assertFalse(followers.isEmpty());

        AsanaIdentity asanaIdentity = followers.get(0);
        Assert.assertEquals(asanaIdentity.getId(), asanaUser.getId());
    }

    @Test(expected = AsanaException.class)
    public void testGetUserWithWrongId() {
        AsanaUser asanaUser = asanaClient.getUser(-1l);
    }
}
