package com.capra.integration.asana;

import com.capra.integration.asana.http.AdvancedAsanaUrlGenerator;
import com.capra.integration.asana.http.AsanaGoogleHttpClient;
import com.capra.integration.asana.http.AsanaHttpClient;
import com.capra.integration.asana.utils.TestConfigurationSource;
import com.capra.integration.asana.utils.TestDataCleanUp;
import com.google.common.collect.Collections2;
import org.junit.After;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import com.capra.integration.asana.http.SimpleAsanaUrlGenerator;
import com.capra.integration.asana.model.*;
import com.capra.integration.asana.utils.AsanaIdentityNamePredicate;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static com.capra.integration.asana.utils.TestConfigurationSource.getWorkspaceId;

/**
 * @author lka@capraconsulting.no
 * @since 24-09-2013 08:37
 */
public class AsanaProjectsTest {

    private final AsanaHttpClient asanaHttpClient = new AsanaGoogleHttpClient(TestConfigurationSource.getConfiguration());
    private final TestDataCleanUp testDataCleanUp = new TestDataCleanUp(asanaHttpClient);


    @After
    public void cleanUp() {
        testDataCleanUp.clean();
    }

    @Test
    public void testReadBatchProjectData() {
        AsanaProjectList asanaProjects = asanaHttpClient.get(AsanaProjectList.class, AdvancedAsanaUrlGenerator.projects().withFields("name", "team").build());
        Assert.assertNotNull(asanaProjects);
    }

    @Test
    public void testCreateReadProject() {
        AsanaProjectCreate asanaProjectCreate = new AsanaProjectCreate("Test Project", getTestTeamId(), getWorkspaceId());
        asanaProjectCreate.setValidColor(AsanaValidColor.DARK_BLUE);
        AsanaProject asanaProject = asanaHttpClient.post(AsanaProject.class, asanaProjectCreate, SimpleAsanaUrlGenerator.projects());
        testDataCleanUp.addProject(asanaProject.getId());

        AsanaData asanaProjectData = asanaHttpClient.get(AsanaJsonData.class, SimpleAsanaUrlGenerator.project(asanaProject.getId()));
        Assert.assertNotNull(asanaProjectData);
    }

    @Test
    public void testMakeProjectArchived() {
        AsanaProjectCreate asanaProjectCreate = new AsanaProjectCreate("Test Project", getTestTeamId(), getWorkspaceId());
        AsanaProject asanaProject = asanaHttpClient.post(AsanaProject.class, asanaProjectCreate, SimpleAsanaUrlGenerator.projects());
        testDataCleanUp.addProject(asanaProject.getId());
        Assert.assertFalse(asanaProject.isArchived());

        AsanaProject forUpdate = new AsanaProject(asanaProject.getId());
        forUpdate.setArchived(true);

        asanaProject = asanaHttpClient.put(AsanaProject.class, forUpdate, SimpleAsanaUrlGenerator.project(asanaProject.getId()));
        Assert.assertTrue(asanaProject.isArchived());
    }

    @Test
    @Ignore
    public void testDeleteProject() {
        AsanaProjectList asanaProjectList = asanaHttpClient.get(AsanaProjectList.class, SimpleAsanaUrlGenerator.projects());
        Collection<AsanaProject> lkProjects = Collections2.filter(asanaProjectList.getData(), new AsanaIdentityNamePredicate("Lukasz Kaleta"));
        for (AsanaProject asanaProject : lkProjects) {
            AsanaTaskList asanaTaskList = asanaHttpClient.get(AsanaTaskList.class, SimpleAsanaUrlGenerator.projectTasks(asanaProject.getId()));
            for (AsanaTask asanaTask : asanaTaskList.getData()) {
                asanaHttpClient.delete(SimpleAsanaUrlGenerator.task(asanaTask.getId()));
            }
            asanaHttpClient.delete(SimpleAsanaUrlGenerator.project(asanaProject.getId()));
        }
    }

    private Long getTestTeamId() {
        AsanaList teamList = asanaHttpClient.get(AsanaList.class, SimpleAsanaUrlGenerator.teams(getWorkspaceId()));
        Collection<AsanaIdentity> teams = Collections2.filter(teamList.getData(), new AsanaIdentityNamePredicate(TestConfigurationSource.getTeamName()));
        Assert.assertFalse(teams.isEmpty());
        return teams.iterator().next().getId();
    }
}
