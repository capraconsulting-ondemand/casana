package com.capra.integration.asana.utils;

import com.capra.integration.asana.AsanaConfiguration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Contains some configuration hardcoded values for tests.
 *
 * @author lka@capraconsulting.no
 * @since 25-09-2013 09:05
 */
public class TestConfigurationSource {

    private static final String ON_BOARDING_TAG;
    private static final Long WORKSPACE_ID;
    private static final String USER_API_KEY;
    private static final String TEAM_NAME;

    static  {
        Class c = TestConfigurationSource.class;
        String propertiesFile = c.getSimpleName() + ".properties";

        InputStream inputStream = c.getResourceAsStream(propertiesFile);
        if (inputStream == null) {
            throw new IllegalStateException("Test Configuration Properties file is missing! Please provide " + propertiesFile + " at classpath: " + c.getPackage().getName());
        }

        Properties properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            throw new IllegalStateException("Can not construct test configuration source: " + e.getMessage(), e);
        }

        if (properties.isEmpty()) {
            throw new IllegalStateException("Your configuration properties file is empty");
        }

        ON_BOARDING_TAG = properties.getProperty("ASANA.ON_BOARDING_TAG");
        String onBoardingTagId = null;
        try {
            onBoardingTagId = properties.getProperty("ASANA.WORKSPACE_ID");
            WORKSPACE_ID = Long.parseLong(onBoardingTagId);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Please check yours workspace id, it should be of type long, but it was: " + onBoardingTagId);
        }

        USER_API_KEY = properties.getProperty("ASANA.USER_API_KEY");

        TEAM_NAME = properties.getProperty("ASANA.TEAM_NAME");
    }

    public static AsanaConfiguration getConfiguration() {
        return new AsanaConfiguration(USER_API_KEY);
    }

    public static Long getWorkspaceId() {
        return WORKSPACE_ID;
    }

    public static String getTeamName() {
        return TEAM_NAME;
    }

    public static String getOnBoardingTag() {
        return ON_BOARDING_TAG;
    }
}
