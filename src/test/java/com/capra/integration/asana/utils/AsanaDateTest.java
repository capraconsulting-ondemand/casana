package com.capra.integration.asana.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

/**
 * @author lka@capraconsulting.no
 * @since 23-09-2013 15:10
 */
public class AsanaDateTest {

    @Test
    public void testName() throws Exception {
        Date date = AsanaDate.parse("2012-02-22T02:06:58.158Z");
        Assert.assertNotNull(date);
    }
}
