package com.capra.integration.asana.utils;

import com.capra.integration.asana.AsanaClient;
import com.capra.integration.asana.http.AsanaHttpClient;
import com.capra.integration.asana.http.SimpleAsanaUrlGenerator;

import java.util.HashSet;
import java.util.Set;

/**
 * @author lka@capraconsulting.no
 * @since 11-11-2013 09:49
 */
public class TestDataCleanUp {

    private final AsanaClient asanaClient;

    private final Set<Long> taskIds = new HashSet<Long>();

    private final Set<Long> projectIds = new HashSet<Long>();

    public TestDataCleanUp(AsanaHttpClient asanaHttpClient) {
        this.asanaClient = new AsanaClient(asanaHttpClient);
    }

    public TestDataCleanUp(AsanaClient asanaClient) {
        this.asanaClient = asanaClient;
    }

    public void addTask(Long taskId) {
        taskIds.add(taskId);
    }

    public void addProject(Long id) {
        projectIds.add(id);
    }

    public void clean() {
        for (Long taskId : taskIds) {
            asanaClient.deleteTask(taskId);
        }
        for (Long projectId : projectIds) {
            asanaClient.deleteProject(projectId);
        }
    }
}
