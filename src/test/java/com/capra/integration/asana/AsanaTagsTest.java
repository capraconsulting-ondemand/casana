package com.capra.integration.asana;

import com.capra.integration.asana.http.AdvancedAsanaUrlGenerator;
import com.capra.integration.asana.http.AsanaGoogleHttpClient;
import com.capra.integration.asana.utils.TestConfigurationSource;
import com.google.common.collect.Collections2;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import com.capra.integration.asana.http.SimpleAsanaUrlGenerator;
import com.capra.integration.asana.model.*;
import com.capra.integration.asana.utils.AsanaIdentityNamePredicate;

import java.util.Collection;
import java.util.List;

import static com.capra.integration.asana.utils.TestConfigurationSource.getWorkspaceId;

/**
 * @author lka@capraconsulting.no
 * @since 23-09-2013 09:46
 */
public class AsanaTagsTest {

    // Id of the onboarding tag in asana: 1907003371869

    private AsanaGoogleHttpClient asanaHttpClient = new AsanaGoogleHttpClient(TestConfigurationSource.getConfiguration());

    @Test
    @Ignore
    // We ignore this test since deleting tags may be forbidden for this user.
    public void testDeleteOnBoardingTags() {
        AsanaList asanaList = asanaHttpClient.get(AsanaList.class, SimpleAsanaUrlGenerator.tags());
        Assert.assertNotNull(asanaList);
        Collection<AsanaIdentity> filter = Collections2.filter(asanaList.getData(), new AsanaIdentityNamePredicate(getOnBoardingTag()));
        for (AsanaIdentity asanaIdentity : filter) {
            asanaHttpClient.delete(SimpleAsanaUrlGenerator.tag(asanaIdentity.getId()));
        }
        asanaList = asanaHttpClient.get(AsanaList.class, SimpleAsanaUrlGenerator.tags());
        Assert.assertNotNull(asanaList);
        filter = Collections2.filter(asanaList.getData(), new AsanaIdentityNamePredicate(getOnBoardingTag()));
        Assert.assertTrue(filter.isEmpty());
    }

    @Test
    public void testQueryingForTags() {
        AsanaList asanaList = asanaHttpClient.get(AsanaList.class, SimpleAsanaUrlGenerator.tags());
        Assert.assertNotNull(asanaList);
    }

    @Test
    public void testSimpleQueryingForTagsInWorkspace() {
        AsanaList asanaList = asanaHttpClient.get(AsanaList.class, SimpleAsanaUrlGenerator.workspaceTags(getWorkspaceId()));
        Assert.assertNotNull(asanaList);
    }

    @Test
    public void testAdvancedQueryingForTagsInWorkspace() {
        AsanaTagList asanaTagList = asanaHttpClient.get(AsanaTagList.class, AdvancedAsanaUrlGenerator
                .workspaceTags(getWorkspaceId())
                .withFields(AsanaTag.NAME, AsanaTag.CREATED_AT)
                .build()
        );
        Assert.assertNotNull(asanaTagList);
        for (AsanaTag asanaTag : asanaTagList.getAsanaTags()) {
            Assert.assertNotNull(asanaTag.getName());
            Assert.assertNotNull(asanaTag.getCreatedAt());
        }
    }

    @Test
    public void testCreateTag() {
        AsanaTagCreate asanaTagCreate = new AsanaTagCreate(getOnBoardingTag(), getWorkspaceId());
        asanaTagCreate.setColor("dark-red");
        asanaTagCreate.setNotes("This tag is for onboarding application");
        AsanaJsonData asanaJsonData = asanaHttpClient.post(AsanaJsonData.class, asanaTagCreate, SimpleAsanaUrlGenerator.tags());
        Assert.assertNotNull(asanaJsonData);
    }

    @Test
    public void testCreateTaskWithATag() {
        AsanaTagCreate asanaTagCreate = new AsanaTagCreate(getOnBoardingTag(), getWorkspaceId());
        asanaTagCreate.setValidColor(AsanaValidColor.getPreferredColor());
        AsanaTag asanaTag = asanaHttpClient.post(AsanaTag.class, asanaTagCreate, SimpleAsanaUrlGenerator.tags());
        AsanaTaskCreate taskCreate = new AsanaTaskCreate(getWorkspaceId(), "task with onboarding tag");
        AsanaTask asanaTask = asanaHttpClient.post(AsanaTask.class, taskCreate, SimpleAsanaUrlGenerator.tasks());
        asanaHttpClient.post(new AsanaTaskTagOperation(asanaTag.getId()), SimpleAsanaUrlGenerator.taskAddTag(asanaTask.getId()));

        AsanaTask asanaTaskWithTag = asanaHttpClient.get(AsanaTask.class, SimpleAsanaUrlGenerator.task(asanaTask.getId()));
        List<AsanaIdentity> tags = asanaTaskWithTag.getTags();
        Collection<AsanaIdentity> expectedTags = Collections2.filter(tags, new AsanaIdentityNamePredicate(getOnBoardingTag()));
        Assert.assertNotNull(expectedTags);
        Assert.assertFalse(expectedTags.isEmpty());
        Assert.assertTrue(expectedTags.size() == 1);
        Assert.assertEquals(expectedTags.iterator().next().getName(), getOnBoardingTag());
    }

    private String getOnBoardingTag() {
        return TestConfigurationSource.getOnBoardingTag();
    }
}
