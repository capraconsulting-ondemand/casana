package com.capra.integration.asana;

import com.capra.integration.asana.http.AsanaGoogleHttpClient;
import com.capra.integration.asana.http.AsanaHttpClient;
import com.capra.integration.asana.http.SimpleAsanaUrlGenerator;
import com.capra.integration.asana.model.*;
import com.capra.integration.asana.utils.TestConfigurationSource;
import com.capra.integration.asana.utils.TestDataCleanUp;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.capra.integration.asana.utils.TestConfigurationSource.getWorkspaceId;

/**
 * @author lka@capraconsulting.no
 * @since 10-11-2013 10:47
 */
public class AsanaTaskStoryTest {

    private final AsanaHttpClient asanaHttpClient = new AsanaGoogleHttpClient(TestConfigurationSource.getConfiguration());
    private final TestDataCleanUp testDataCleanUp = new TestDataCleanUp(asanaHttpClient);

    @After
    public void doAfterTest() {
        testDataCleanUp.clean();
    }

    @Test
    public void testAddCommentToTheTask() {
        AsanaTaskCreate asanaTaskCreate = new AsanaTaskCreate(getWorkspaceId(), "storyTask");
        AsanaTask asanaTask = asanaHttpClient.post(AsanaTask.class, asanaTaskCreate, SimpleAsanaUrlGenerator.tasks());
        final long taskId = asanaTask.getId();
        testDataCleanUp.addTask(taskId);

        final String comment = "some text";
        AsanaStoryCreate asanaStoryCreate = new AsanaStoryCreate(taskId, comment);
        AsanaStory asanaStory = asanaHttpClient.post(AsanaStory.class, asanaStoryCreate, SimpleAsanaUrlGenerator.taskComment(taskId));
        Assert.assertEquals(asanaStory.getText(), comment);
    }

    @Test
    public void testBrowseCommentsOnTheTask() {
        AsanaTaskCreate asanaTaskCreate = new AsanaTaskCreate(getWorkspaceId(), "storyTask");
        AsanaTask asanaTask = asanaHttpClient.post(AsanaTask.class, asanaTaskCreate, SimpleAsanaUrlGenerator.tasks());
        final long taskId = asanaTask.getId();
        testDataCleanUp.addTask(taskId);

        String[] comments = {"comment 1", "comment 2", "comment 3"};
        for (String comment : comments) {
            AsanaStoryCreate asanaStoryCreate = new AsanaStoryCreate(taskId, comment);
            asanaHttpClient.post(asanaStoryCreate, SimpleAsanaUrlGenerator.taskComment(taskId));
        }

        AsanaStoryList asanaStoryList = asanaHttpClient.get(AsanaStoryList.class, SimpleAsanaUrlGenerator.taskStories(taskId));
        Assert.assertNotNull(asanaStoryList);
        List<AsanaStory> asanaStories = asanaStoryList.getStories();
        Assert.assertTrue(asanaStories.size() == comments.length);
    }
}
